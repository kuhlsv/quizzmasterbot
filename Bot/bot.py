#!/usr/bin/python3
#-*- coding:utf-8 -*-
# =======================================================
# Copyright 2020 Sven Kuhlmann
# =======================================================
"""Bot class and Command class

Command is an class that holds the name and a action as function for an text command.

Bot includes all functions to create an Telegram bot.
It is abstract by {abs.ABC} and need a child class that inherit from it.
The child needs to call the parent constructor and run {self.register_commands()} to 
create the CommandHandlers of all Methods from the Parent. {self.run()} starts the bot.

https://api.telegram.org/bot
"""

import os
import sys
import time
import psutil
import logging
from abc import ABC
from random import randint
from telegram.error import NetworkError
from telegram.ext import Updater, InlineQueryHandler, CommandHandler
from telegram.ext import CallbackContext, MessageHandler, Filters

class Command():

    _name = ""
    _action = None
    
    def __init__(self, name, action):
        self._name = name
        self._action = action

    def get_name(self):
        return self._name
    
    def get_action(self):
        return self._action

class Bot(ABC):

    _TIMEOUT = 12
    _INTERVAL = 6

    _token = None
    _updater = None
    _commands = []

    def __init__(self, token):
        super().__init__()
        self._token = token
        self._updater = Updater(token, use_context=True)
        self.add_error_handler()

    def run(self):
        try:
            self._updater.start_polling(poll_interval=self._INTERVAL, timeout=self._TIMEOUT)
            self._updater.idle()
        except NetworkError as e:
            logging.error(e)

    def stop(self, db=None):
        self.get_updater().stop()
        self.get_updater().is_idle = False
        try:
            db._session.close_all_sessions()
        except Exception:
            logging.error("Cannot close db!")

    def restart(self, wait=0, db=None):
        self.stop(db)
        time.sleep(wait)
        os.execl(sys.executable, sys.executable, *sys.argv)

    def register_commands(self):
        # Get a list of all functions from the child (self) and generate the commands list
        self.generate_commands()
        # Register all commands as CommandHandler
        if(self.get_commands().__len__() < 1):
            raise Exception("No commands in parent class defined!")
        for command in self.get_commands():
                self.add_handler(CommandHandler(command.get_name(), command.get_action()))
                logging.debug("Found command: %s", command.get_name())

    def generate_commands(self):
        # Get all callable functions with pattern command_*
        methods = [func for func in dir(self) if callable(getattr(self, func))]
        for func in methods:
            splitted = func.split("_")
            if splitted[0] == "command":
                self.add_command(Command(splitted[1], getattr(self, func)))

    def register_message_handler(self, filters, callback):
        message_handler = MessageHandler(filters=filters, callback=callback) 
        self.add_handler(message_handler)

    def add_handler(self, handler):
        # The same for all Groups -> 0
        self.get_updater().dispatcher.add_handler(handler, 0)

    def add_error_handler(self):
        self.get_updater().dispatcher.add_error_handler(self.error_callback)

    def get_updater(self):
        return self._updater

    def get_commands(self):
        return self._commands

    def add_command(self, action : Command):
        self._commands.append(action)

    def get_token(self):
        return self._token
    
    def set_token(self, token):
        self._token = token

    def error_callback(self, update, context):
        """ Responses with the error message. Sometimes send a meme! """
        logging.error("Update '%s' caused error '%s'", update, context.error)
        try:
            num = randint(0,6)
            if num == 0:
                file = open("Media/meme-errors.jpg", "rb")
                update.message.reply_photo(photo=file)
        except Exception:
            logging.error("Cannot send error-meme!")
        update.message.reply_text("Ups, something went wrong!\n'" + str(context.error)[:15] + "'")