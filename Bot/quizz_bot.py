#!/usr/bin/python3
#-*- coding:utf-8 -*-
# =======================================================
# Copyright 2020 Sven Kuhlmann
# =======================================================
"""Quizz Bot class

This class inherit from {Bot} and call his constructor with a token.
Also he runs the {Bot.register_commands} function in {__init__} to generate the 
Commands from Telegram and register them. The Commands are generated automaticly for all Methods of this class
with the Pattern command_* (command_help).

Call {QuizzBot.run()} to start the bot.

"""

import re
import logging
import requests
import threading
from Model import *
from DB.base import Base
from threading import Thread
from telegram.ext import Filters
from sqlalchemy.orm import Session

from Bot.bot import Bot

class QuizzBot(Bot):

    _ALLOWED_EXTENSIONS = ["jpg", "jpeg", "png"]
    _EMOJIS = {
        "thunderstorm": u'\U0001F4A8',
        "drizzle": u'\U0001F4A7',
        "rain": u'\U00002614',
        "snowflake": u'\U00002744',
        "snowman": u'\U000026C4',
        "atmosphere": u'\U0001F301',
        "clearSky": u'\U00002600',
        "fewClouds": u'\U000026C5',
        "clouds": u'\U00002601',
        "hot": u'\U0001F525',
        "defaultEmoji": u'\U0001F300'
    }

    _admins = []
    _groups = None
    _db = None

    def __init__(self, db, token, admins):
        super().__init__(token)
        dispatcher = self.get_updater().dispatcher
        self._groups = dispatcher.groups
        self._admins = admins
        self._db = db
        # Register handlers. Order is important
        self.register_commands()
        self.register_handlers()

    def register_handlers(self):
        #self.register_message_handler(Filters.poll, self.poll) #need update package
        self.register_message_handler(Filters.video, self.video)
        self.register_message_handler(Filters.voice, self.voice)
        self.register_message_handler(Filters.audio, self.audio)
        self.register_message_handler(Filters.sticker, self.sticker)
        self.register_message_handler(Filters.photo, self.photo)
        self.register_message_handler(Filters.game, self.game)
        self.register_message_handler(Filters.location, self.location)
        self.register_message_handler(Filters.forwarded, self.forwarded)
        self.register_message_handler(Filters.status_update, self.status)
        self.register_message_handler(Filters.text & Filters.group, self.group_text)
        self.register_message_handler(Filters.text & Filters.private, self.private_text)
        self.register_message_handler(Filters.all & Filters.group, self.group_all)
        self.register_message_handler(Filters.all & Filters.private, self.private_all)
        logging.debug("Registered Handlers")

    def check_admin(self, user):
        return user in self._admins

    def check_group(self, group):
        return group in self._groups

    """ 
    Messages
    """

    def forwarded(self, update, context):
        logging.debug("Got forwarded message")

    def video(self, update, context):
        logging.debug("Got video message")

    def game(self, update, context):
        logging.debug("Got game message")

    def sticker(self, update, context):
        logging.debug("Got sticker message")

    def photo(self, update, context):
        logging.debug("Got photo message")

    def poll(self, update, context):
        logging.debug("Got poll message")

    def audio(self, update, context):
        logging.debug("Got audio message")

    def voice(self, update, context):
        logging.debug("Got voice message")

    def location(self, update, context):
        logging.debug("Got location message")

    def status(self, update, context):
        logging.debug("Got status update message")
        new_user = update.message.new_chat_members
        update.message.chat.send_message("Welcome! :)")
        if (not new_user == None) and (not new_user == []):
            for user in new_user:
                self.new_user(user)
    
    def group_text(self, update, context):
        logging.debug("Got group txt message")
        chat = self.get_or_add(update.message.chat)
        # Message
        if(bool(update.message.text)):
            user = self.get_or_add(update.message.from_user)
            logging.debug(user)
            message = self.get_or_add(update.message, chat.id, user.id)
            logging.debug(message)

    def private_text(self, update, context):
        logging.debug("Got private txt message")   
        chat = self.get_or_add(update.message.chat)
        # Message
        if(bool(update.message.text)):
            user = self.get_or_add(update.message.from_user)
            logging.debug(user)
            message = self.get_or_add(update.message, chat.id, user.id)
            logging.debug(message)

    def private_all(self, update, context):
        logging.debug("Got private all message")

    def group_all(self, update, context):
        logging.debug("Got group all message")
        logging.debug(update.message)
        # Poll
        if(bool(update.message.poll)):
            user = self.get_or_add(update.message.from_user)
            logging.debug(user)
            date = self.parse_date(update.message.date)
            poll = self.get_or_add(update.message.poll, user.id, date)
            logging.debug(poll)
            for opt in update.message.poll.options:
                option = self.get_or_add(opt, poll.id)
                logging.debug(option) # TODO Reply to every second anonymus quizzes

    """ 
    Commands
    """

    def command_commands(self, update, context):
        """ 
        Get an info about all possible commands 
        """
        exclude_list = ["start"]
        commands = self.get_commands()
        text = "Hello {}, you have the following options:\n".format(update.message.from_user.first_name)
        for command in commands:
            if command.get_name() not in exclude_list:
                text += "/" + command.get_name() + "\n"
        update.message.reply_text(text)

    def command_info(self, update, context):
        """ 
        Get an info
        """
        text = "I am the Bot for the groups @Chatquizz and @Nerdquiz.\n"+\
            "I manage these and give them some extra favor. Also i eject a kind of the group every Week/Month.\n"+\
            "Mostly im passive. But when you wanna talk to me, than type /commands to see some interactions."
        update.message.reply_text(text)

    def command_elect(self, update, context):
        """ 
        Elect the quiz master king in all groups
        """
        if(update.message.from_user.username in self._admins):
            print("TODO")

    def command_restart(self, update, context):
        """ 
        Restart bot by Admin 
        """
        if(update.message.from_user.username in self._admins):
            update.message.reply_text("Bot is restarting...")
            time = 0
            try:
                time = int(update.message.text.partition(" ")[2])
            except Exception:
                logging.info("Cannot restart with " + update.message.text)
         	    #threading.Thread(target=self.restart, args=(time,self._db,)).start()
        else:
            update.message.reply_text("Sry, you are not an admin!")


    def command_stop(self, update, context):
        """ 
        Stop bot by Admin 
        """
        if(update.message.from_user.username in self._admins):
            update.message.reply_text("Bot is stopping...")
            #threading.Thread(target=self.stop, args=(self._db,)).start()
        else:
            update.message.reply_text("Sry, you are not an admin!")

    """
    Helper
    """

    def new_user(self, tg_user):
        """ It handles new users. """
        text = "Hello "+tg_user.first_name+", I am the QuizMaster for the Group you recently joined!\n"+\
            "I will just inform you about the Group: The group uses the new verions of polls of Telegram.\n"+\
            "You can now create a quiz-poll by checking a option under the question and then select the right answer! :)\n"+\
            "Look into the group-status for more infos or contact me by wirting: /info \n"+\
            "I also elect the king of the quizzes, the quiz master, every week/month. See you in the group! :D"
        # Contact new users
        file = open("Media/quiz1.jpg", "rb")
        tg_user.send_photo(photo=file)
        tg_user.send_message(text=text)


    def get_or_add(self, tg_item, arg1=None, arg2=None):
        """ Get TG item from db or create & add it. 
        Some classes need a dependencies as params (may user.id) """
        # Check id -> PollOption
        if not (hasattr(tg_item, "id")):
            setattr(tg_item, "id", None)
        # Get class from Model module
        model_class = self.get_model_class(tg_item)
        # Try get from db
        result = self._db.query(model_class, tg_item.id, False)
        if(result == None):
            # Create to db
            item = model_class.create_from_tg(tg_item, arg1, arg2)
            valid = self._db.insert(item)
            if(valid):
                result = item
            # Contact new users
            if model_class.__name__ == "User":
                self.new_user(tg_item)
        return result

    def get_model_class(self, tg_item):
        """ Returns the claass from the referenz-model of a TG-class """
        type_class = type(tg_item).__name__            
        #model_class = "Model." + type_class.lower() + "." + type_class
        return eval(type_class) #eval() is security risky. Inection!

    def parse_date(self, datetime):
        try:
            date = datetime.strftime("%m/%d/%Y,%H:%M:%S")
        except Exception:
            date = datetime
        return date

    def kick(self, chat_member):
        print() #TODO

    def elect_quizmaster(self, chat_member):
        print() #TODO

    def flag(self, country_code):
        offset = 127462 - ord('A')
        code = country_code.upper()
        return chr(ord(code[0]) + offset) + chr(ord(code[1]) + offset)

    """ 
    Static
    """

    @staticmethod
    def get_extension_from_url(url):
        return re.search("([^.]*)$", url).group(1).lower()
