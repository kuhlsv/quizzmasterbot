import yaml
import os.path
from pathlib import Path

config_file = "settings.yml"

def get_config():
    dir = Path().absolute()
    return yaml.safe_load(open(os.path.join(str(dir), config_file))) # TODO SettingsException

def get_token():
    return get_config()["token"]

def get_db():
    return get_config()["db"]

def get_admins():
    return get_config()["admins"]