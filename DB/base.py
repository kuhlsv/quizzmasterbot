import re
import datetime
from sys import stdout
from sqlalchemy.ext.declarative import declarative_base

# Entity base
Base = declarative_base()

# Emoji reencoding for DB -> emoji_pattern.sub(r'', text)
emoji_pattern = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
        u"\U00002702-\U000027B0"  # ...
        u"\U00002702-\U000027B0"
        u"\U000024C2-\U0001F251"
        u"\U0001f926-\U0001f937"
        u"\U00010000-\U0010ffff"
        u"\u2640-\u2642" 
        u"\u2600-\u2B55"
        u"\u200d"
        u"\u23cf"
        u"\u23e9"
        u"\u231a"
        u"\ufe0f"
        u"\u3030" 
        "]+", flags=re.UNICODE)

def emoji_decode(text):
        """ Decodes a string for db by removing emojis """
        text = emoji_pattern.sub(r':)', str(text))
        text = text.encode(stdout.encoding, errors="replace")
        return text

# Date
def get_date_now():
        return datetime.datetime.now()