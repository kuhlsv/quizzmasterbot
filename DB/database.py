﻿import logging
from DB.base import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session, Session
from sqlalchemy_utils.functions import database_exists, create_database

# TODO -> Change work with session to from docs:
"""
from contextlib import contextmanager
@contextmanager
def session_scope():
    #Provide a transactional scope around a series of operations.
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()
def run_my_program():
    with session_scope() as session:
        ThingOne().go(session)
        ThingTwo().go(session)
"""

class Database:

    _engine = None
    _session = None

    def __init__(self, connection_str):
        if not database_exists(connection_str):
            create_database(connection_str)
        self._engine = create_engine(
            connection_str,
            encoding="utf8",
            echo=True) #latin1
        self._session = scoped_session(sessionmaker(bind=self._engine))
        Base.metadata.create_all(self._engine)
        
    def get_session(self):
        session = self._session()
        try:
            pass
            #session.commit() #Bad solution
        except Exception:
            session.rollback()
            session = self._session()
        return session

    def query(self, _class, _id, all=True):
        if all:
            result = self.get_session().query(_class).filter_by(id=_id).all()
        else:
            result = self.get_session().query(_class).filter_by(id=_id).first()
        return result

    def insert(self, item):
        self.get_session().add(item)
        self._flush()
        return item

    def insert_all(self, item_array):
        self.get_session().add_all(item_array)
        self._flush()
        return item_array

    def delete(self, item):
        self.get_session().delete(item)
        self._flush()
        return item

    def _flush(self):
        self.get_session().commit()