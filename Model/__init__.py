"""
from os.path import dirname, basename, isfile, join
import glob

# Get all classes into __all__
# This script is a security risk because it would also load malicious classes
modules = glob.glob(join(dirname(__file__), "*.py"))
__all__ = [ basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]
"""
from Model.assocation import User_Chat
from Model.chat import Chat
from Model.message import Message
from Model.poll_option import PollOption
from Model.poll import Poll
from Model.user import User
from Model.vote import Vote

__all__ = ["User_Chat", "Chat", "Message", "PollOption", "Poll", "User", "Vote"]