from DB.base import Base
from Model.chat import Chat
from Model.user import User
from sqlalchemy import Column, Integer, BigInteger, ForeignKey
from sqlalchemy.orm import relationship

class User_Chat(Base):

    # Table
    __tablename__ = "user_chat_assocation"
    # Columns
    user_id = Column(BigInteger, ForeignKey("user.id"), primary_key=True)
    chat_id = Column(BigInteger, ForeignKey("chat.id"), primary_key=True)
    # Relations
    user = relationship("User", back_populates="chats")
    chat = relationship("Chat", back_populates="users")