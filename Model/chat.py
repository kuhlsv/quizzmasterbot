from sys import stdout
from DB.base import Base, emoji_decode, get_date_now
from Model.user import User
from sqlalchemy import Column, Integer, BigInteger, String, Boolean, ForeignKey, Date
from sqlalchemy.orm import relationship

class Chat(Base):
    
    # Table
    __tablename__ = "chat"
    # Columns
    id = Column(BigInteger, primary_key=True, autoincrement=False)
    username = Column(String(100))
    ctype = Column(String(50))
    title = Column(String(100))
    description = Column(String(512))
    invite_link = Column(String(100))
    sticker_set = Column(String(100))
    created_at = Column(Date, default=get_date_now)
    updated_at = Column(Date, onupdate=get_date_now)
    # Relations
    message = relationship("Message")
    users = relationship("User_Chat", back_populates="chat")

    @staticmethod
    def create_from_tg(tg_chat, arg1=None, arg2=None):
        # chat_id dont have to be set
        # Normalisize
        title = emoji_decode(tg_chat.title)
        description = emoji_decode(tg_chat.description)
        return Chat(
                id=tg_chat.id,
                username=tg_chat.username,
                ctype=tg_chat.type,
                title=title,
                description=description,
                invite_link=tg_chat.invite_link,
                sticker_set=tg_chat.sticker_set_name)