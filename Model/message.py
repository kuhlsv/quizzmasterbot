from sys import stdout
from DB.base import Base, emoji_decode, get_date_now
from Model.chat import Chat
from Model.user import User
from sqlalchemy import Column, Integer, BigInteger, String, Boolean, ForeignKey, Date
from sqlalchemy.orm import relationship

class Message(Base):
    
    # Table
    __tablename__ = "message"
    # Columns
    id = Column(BigInteger, primary_key=True, autoincrement=False)
    user_id = Column(BigInteger, ForeignKey("user.id"))
    chat_id = Column(BigInteger, ForeignKey("chat.id"))
    text = Column(String(1024))
    created_at = Column(Date, default=get_date_now)
    updated_at = Column(Date, onupdate=get_date_now)
    
    @staticmethod
    def create_from_tg(tg_message, chat_id=None, user_id=None):
        # message_id dont have to be set
        # Normalisize
        text = emoji_decode(tg_message.text)
        return Message(
                id=tg_message.message_id,
                text=text,
                chat_id=chat_id,
                user_id=user_id)