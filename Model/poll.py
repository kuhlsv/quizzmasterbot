from sys import stdout
from DB.base import Base, emoji_decode, get_date_now
from Model.user import User
from Model.poll_option import PollOption
from sqlalchemy import Column, Integer, BigInteger, String, Boolean, ForeignKey, Date
from sqlalchemy.orm import relationship
    
class Poll(Base):
    
    # Table
    __tablename__ = "poll"
    # Columns
    id = Column(BigInteger, primary_key=True, autoincrement=False)
    user_id = Column(BigInteger, ForeignKey("user.id"))
    ptype = Column(String(50))
    question = Column(String(512))
    is_closed = Column(Boolean)
    date = Column(String(50))
    created_at = Column(Date, default=get_date_now)
    updated_at = Column(Date, onupdate=get_date_now)
    # Relations
    correct_option = relationship("PollOption", uselist=False, back_populates="correct_poll")
    options = relationship("PollOption")
    vote = relationship("Vote")

    @staticmethod
    def create_from_tg(tg_poll, user_id, date=None):
        # Normalisize
        question = emoji_decode(tg_poll.question)
        #options = [Option.create_from_tg(o) for o in tg_poll.options]
        #correct_option = (o for o in options if o.id == tg_poll.correct_option_id)
        return Poll(
                id=tg_poll.id,
                ptype=None,
                date=date,
                question=question,
                is_closed=tg_poll.is_closed,
                user_id=user_id)