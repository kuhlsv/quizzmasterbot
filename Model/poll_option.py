from sys import stdout
from DB.base import Base, emoji_decode, get_date_now
from sqlalchemy import Column, Integer, String, BigInteger, Boolean, ForeignKey, Date
from sqlalchemy.orm import relationship

class PollOption(Base):
    
    # Table
    __tablename__ = "polloption"
    # Columns
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    poll_id = Column(BigInteger, ForeignKey("poll.id"))
    text = Column(String(512))
    voter_count = Column(Integer)
    created_at = Column(Date, default=get_date_now)
    updated_at = Column(Date, onupdate=get_date_now)
    # Relations
    correct_poll = relationship("Poll", back_populates="correct_option")
    vote = relationship("Vote")

    @staticmethod
    def create_from_tg(tg_option, poll_id, arg2=None):
        # Normalisize
        text = emoji_decode(tg_option.text)
        return PollOption(
                text=text,
                voter_count=tg_option.voter_count,
                poll_id=poll_id)