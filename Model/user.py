from sys import stdout
from DB.base import Base, emoji_decode, get_date_now
from sqlalchemy import Column, Integer, String, BigInteger, Boolean, ForeignKey, Date
from sqlalchemy.orm import relationship

class User(Base):
    
    # Table
    __tablename__ = "user"
    # Columns
    id = Column(BigInteger, primary_key=True, autoincrement=False)
    username = Column(String(100))
    name = Column(String(100))
    first_name = Column(String(100))
    last_name = Column(String(100))
    link = Column(String(100))
    is_bot = Column(Boolean)
    created_at = Column(Date, default=get_date_now)
    updated_at = Column(Date, onupdate=get_date_now)
    # Relations
    poll = relationship("Poll")
    vote = relationship("Vote")
    message = relationship("Message")
    chats = relationship("User_Chat", back_populates="user")
            
    @staticmethod
    def create_from_tg(tg_user, arg1=None, arg2=None):
        # user_id dont have to be set
        # Normalisize
        username = emoji_decode(tg_user.username)
        name = emoji_decode(tg_user.name)
        first_name = emoji_decode(tg_user.first_name)
        last_name = emoji_decode(tg_user.last_name)
        return User(
                id=tg_user.id,
                name=name,
                username=username,
                first_name=first_name,
                last_name=last_name,
                link=tg_user.link,
                is_bot=tg_user.is_bot)