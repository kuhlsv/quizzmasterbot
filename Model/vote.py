from DB.base import Base, get_date_now
from Model.user import User
from Model.poll import Poll
from Model.poll_option import PollOption
from sqlalchemy import Column, Integer, String, BigInteger, Boolean, ForeignKey, Date
from sqlalchemy.orm import relationship

class Vote(Base):
    
    # Table
    __tablename__ = "vote"
    # Columns
    id = Column(BigInteger, primary_key=True, autoincrement=False)
    option_id = Column(BigInteger, ForeignKey("polloption.id"))
    user_id = Column(BigInteger, ForeignKey("user.id"))
    poll_id = Column(BigInteger, ForeignKey("poll.id"))
    is_right_option = Column(Boolean)
    created_at = Column(Date, default=get_date_now)
    updated_at = Column(Date, onupdate=get_date_now)

    @staticmethod
    def create_from_tg(tg_vote, user_id=None, poll_id=None):
        return Vote(
                id=tg_vote.id,
                option_id=tg_vote.option.id,
                user_id=user_id,
                poll_id=poll_id,
                is_right_option=tg_vote.is_right_option)