# QuizzMasterBot

Bot for the Telegram Groups -> NerdQuiz and ChatQuizz

## Dependecie
Uses [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) module

## Settings/Config
Settigns getting read by `config_reader`.<br>
In the `settings.yml` (not included, hast to be in proj-root-dir) are defined:
* token: API Token
* db: DB Connection String
* admins: A list of usernames with admin rights for the bot

## Access
You can add/contact the Bot by writing @QuizzMasterBot in TG