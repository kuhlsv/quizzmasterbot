import os
import logging
import Config.config_reader as cfg
from Model.poll import Poll
from Bot.quizz_bot import QuizzBot
from DB.database import Database

logdir = "Log/"
logfile = "bot.log"

def main():
    # Check log size
    cleanup()
    # Init log
    logging.basicConfig(filename=logdir + logfile,
                        format="%(asctime)-15s - %(levelname)s: %(message)s",
                        level=logging.DEBUG)
    # Read token from config
    token = cfg.get_token()
    # Init db
    db = Database(cfg.get_db())
    # Start bot
    QuizzBot(db, token, cfg.get_admins()).run()

def cleanup():
    del_size = 4522420
    for file in os.listdir(logdir):
        if file.endswith(".log"):
            if os.path.getsize(logdir + file) > del_size:
                os.remove(logdir + file)

if __name__ == "__main__":
    main()